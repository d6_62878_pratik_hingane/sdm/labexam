const mysql2 = require('mysql')

const Pool = mysql2.createPool({
  connectionLimit: 20,
  user: 'root',
  password: 'password',
  database: 'labexam',
  port: 3306,
  host: 'localhost',
  waitForConnections: true,
  queueLimit: 0,
})

module.exports = {
  Pool,
}